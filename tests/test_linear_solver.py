import pytest
import numpy as np
from linear_solver import LinearSolver
import linear_solver

coefficiants=[-4,5]
coefficiants_class=[2,4,-3]
lin_solver = LinearSolver(coefficiants,80)

def test_generate_populations_shape():
    lin_solver.generate_populations()
    assert lin_solver.populations.shape[0] == 80
    assert lin_solver.populations.shape[1] == 2

def test_generate_populations_values():
    lin_solver.generate_populations()
    assert linear_solver._LOWER_LIMIT<=min(lin_solver.populations[:, 0])
    assert linear_solver._UPPER_LIMIT>=max(lin_solver.populations[:, 0])

def test_calc_fitness_score_for_population():
    result = lin_solver.calc_fitness_score_for_population(np.asarray([[0,1],[1,1],[2,2]]),coefficiants)
    # result = result[result[:, -1].argsort()]
    assert result[0][0] == 0
    assert result[0][2] == 5
    assert result[1][2] == 1
    assert result[2][2] == 2

def test_select_parents_top_1():
    population_with_fitness = lin_solver.calc_fitness_score_for_population(np.asarray([[2, 2],[0, 1], [1, 1]]), coefficiants)
    result = lin_solver.select_parents(population_with_fitness,0.34)
    assert result.shape[0] == 1
    assert result[0][0] == 0
    assert result[0][1] == 1

def test_select_parents_second():
    population_with_fitness = lin_solver.calc_fitness_score_for_population(np.asarray([[0, 1], [1, 1], [2, 2]]), coefficiants)
    result = lin_solver.select_parents(population_with_fitness,0.67)
    assert result.shape[0] == 2
    assert result[1][0] == 2
    assert result[1][1] == 2

def test_select_random_parents():
    population_with_fitness = lin_solver.calc_fitness_score_for_population(np.asarray([[2, 2],[0, 1], [1, 1], [4, 5], [-7, 8], [10, 10], [7.6, -8], [3, 9]]), coefficiants)
    select_parents = lin_solver.select_parents(population_with_fitness,0.8)
    result = lin_solver.choose_parents_to_crossover(select_parents)
    assert result.shape[0] == 2

def test_crossover():
    parent_1 = [2,3,4,5,6,7,8]
    parent_2 = [1,1,1,1,1,1,1]
    result = lin_solver.crossover(parent_1,parent_2)
    assert (parent_1 == result).all() == False
    assert (parent_2 == result).all() == False

def test_mutate():#NOT WORKING!!
    chromosome = [2,3,4,5,6,7,8]
    lin_solver.mutate(chromosome)
    assert chromosome != [2,3,4,5,6,7,8]

def test_run():
    coefficiants = [2,4,-3]
    lin_solver_final = LinearSolver(coefficiants, 80)
    lin_solver_final.run(total_generations=70, parent_selection_rate=0.2,mutation_rate=0.0)

