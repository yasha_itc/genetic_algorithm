import numpy as np

_LOWER_LIMIT = -10
_UPPER_LIMIT = +10

class LinearSolver:

    def __init__(self,coefficients_array,initial_population_size):
        """
        Initiates the LinearSolver class
        :param coefficients_array list(float): An array that represents the coefficients of the linear equation
                                               we want to find solution to
        :param initial_population_size int: The size of the initial population
        """
        self.coefficients_array = coefficients_array
        self.initial_population_size = initial_population_size
        self.populations = None

    def run(self, total_generations, parent_selection_rate, mutation_rate):
        """
        :param total_generations int: number of iterations of selection and crossover
        :param parent_selection_rate float: a number in the range (0,1) that indicates the percentage of parents we want
                                            to select for "breeding"
        :param mutation_rate float: a number in the range (0,1) that indicates the percentage of offsprings that
                                     will have a mutation
        :return:
        """
        self.generate_populations()
        for i in range(total_generations):
            population_with_fitness = self.calc_fitness_score_for_population(self.populations,self.coefficients_array)
            selected_parents_with_fitness = self.select_parents(population_with_fitness,parent_selection_rate)
            print("best fitted in "+str(i)+" iteration is "+str(selected_parents_with_fitness[0]))
            selected_parents = np.delete(selected_parents_with_fitness, -1, axis=1)
            #appending first row for technical reasons
            children = np.asarray([np.zeros(len(self.coefficients_array))])
            while (selected_parents.shape[0] + children.shape[0] <self.initial_population_size+1):
                parents_to_crossover = self.choose_parents_to_crossover(selected_parents)
                child_chromosome = self.crossover(list(parents_to_crossover[0]),list(parents_to_crossover[1]))
                if mutation_rate<np.random.uniform():
                    self.mutate(child_chromosome)
                children = np.append(children, [child_chromosome], axis=0)
            self.populations = np.append(selected_parents, children[1:], axis=0)

    def generate_populations(self):
        """
        Generates a random VALID population, stores it as a ndarray where each row is a chromosome
        and each cell is a gene
        """
        self.populations = np.random.uniform(low=_LOWER_LIMIT, high=_UPPER_LIMIT, size=(self.initial_population_size,len(self.coefficients_array)))

    def calc_fitness_score_for_population(self,populations,coefficients_array):
        """
         Calculates the fitness score for every chromosome
        :param populations ndarray: ndarray of populations
        :param coefficients_array list(float):  array of equation coefficients
        :return ndarray: populations ndarray with a column of fitness scores appended to it
        """
        fitness_results = np.sum(populations*coefficients_array, axis=1)
        return np.append(populations,np.expand_dims(fitness_results,1), axis=1)

    def select_parents(self,population_with_fitness,selection_rate):
        """
        Returns the top X chromosomes by fitness score, where x is selection_rate
        :param population_with_fitness ndarray: populations ndarray with a column of fitness scores appended to it
        :param selection_rate float: a number in the range (0,1) that indicates the percentage of parents we want
                                     to select for "breeding"
        :return ndarray: subset of population_with_fitness of parents selected for breeding
        """
        rows_to_select = int(population_with_fitness.shape[0]*selection_rate)
        sorted_indexes_by_last_col = population_with_fitness[:, -1].argsort()
        population_sorted_desc = population_with_fitness[sorted_indexes_by_last_col][::-1]
        return population_sorted_desc[:rows_to_select]


    def choose_parents_to_crossover(self,parents):
        """
        chooses randomly 2 parents to do crossover on
        :param parents ndarray: parents selected for breeding WITHOUT fitness score
        :return ndarray: returns 2 parents to breed, dimensions are 2xcoefficients_array_size
        """
        rand_indexes = np.random.randint(0, parents.shape[0], 2)
        return parents[rand_indexes]

    def crossover(self,parent_1,parent_2):
        """
        returns an offspring chromosome that is a crosover of parent_1 and parent_2 using *uniform* crossover
        :param parent_1 ndarray: first parent to crossover
        :param parent_2 ndarray: second parent to crossover
        :return ndarray: offspring of the 2 parents
        """
        genes_of_parent_1 = np.random.randint(0,2,len(parent_1))
        genes_of_parent_2 = 1 - genes_of_parent_1
        return parent_1*genes_of_parent_1+parent_2*genes_of_parent_2

    def mutate(self,chromosome):
        """
        This method enlarges a single random gene by 10%, no more than the _UPPER_LIMIT
        , no less than the _LOWER_LIMIT
        :param chromosome: the chromosome we want to mutate
        :return ndarray: mutated chromosome
        """
        rand_index = np.random.randint(0, len(chromosome))
        if chromosome[rand_index]*1.1> _UPPER_LIMIT:
            chromosome[rand_index] = _UPPER_LIMIT
        elif chromosome[rand_index]*1.1<_LOWER_LIMIT:
            chromosome[rand_index] = _LOWER_LIMIT
        else:
            chromosome[rand_index] = chromosome[rand_index]*1.1
        return chromosome
